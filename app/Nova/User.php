<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'phone','address'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            // ID::make()->sortable(),

            // Gravatar::make()->maxWidth(50),

            Text::make('nom', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('phone')
                ->sortable()
                ->rules('required', 'string', 'max:254')
                ->creationRules('unique:users,phone')
                ->updateRules('unique:users,phone,{{resourceId}}'),

             Text::make('address')
                ->sortable()
                ->rules('nullable', 'string', 'max:254')
                ->creationRules('nullable', 'string', 'max:254')
                ->updateRules('nullable', 'string', 'max:254'),

            Password::make('password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:4')
                ->updateRules('nullable', 'string', 'min:4'),

            Select::make('Profiles','profile_id')->onlyOnForms()->options(\App\Models\Profiles::pluck('name', 'id')),
            BelongsTo::make('Category')->withSubtitles()->showCreateRelationButton(),
            BelongsTo::make('Team')->withSubtitles()->showCreateRelationButton(),
            BelongsToMany::make('Products'),
            HasMany::make('suscriptions'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
