<?php

namespace App\Http\Resources;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $balance = DB::table('orders')
                    ->where('user_id',$this->id)
                    ->whereBetween('created_at', [Carbon::today()->setTime(6, 00, 00), Carbon::tomorrow()->setTime(5, 59, 59)])
                    ->sum('amount');
        return [
            'id'          => $this->id,
            'profile_id'  => $this->profile_id,
            'team_id'     => $this->team_id,
            'category_id' => $this->category_id,
            'name'        => $this->name,
            'phone'       => $this->phone,
            'address'     => $this->address,
            'email'       => $this->email,
            'balance'     => $balance,
        ];
    }
}
