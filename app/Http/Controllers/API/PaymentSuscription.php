<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\PaydunyaSenegalIntegration;
use App\Http\Controllers\Controller;

class PaymentSuscription extends Controller
{
    public function initPayment(Request $request)
    {
    	$input = $request->all();
   
        $validator = Validator::make($input, [
            'phone'        	   => 'required',
            'ussd_code'    	   => '',
            'payment_method'   => 'required'
        ]);

        if ($input['payment_method'] == "wave-senegal") 

        	return PaydunyaSenegalIntegration::waveIntegration($input['phone'],Auth::user()->phone);
        	
        if ($input['payment_method']== "orange-money-senegal") 

        	return PaydunyaSenegalIntegration::orangeMoneyIntegration($input['phone'],$input['ussd_code'],Auth::user()->phone);
        	
    }
}
