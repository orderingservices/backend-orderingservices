<?php
   
namespace App\Http\Controllers\API;

use Hash;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Suscription;
use App\Models\Profiles;
use Illuminate\Http\Request;
use App\Services\Suscriptions;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'phone'         => 'required',
            'address'       => 'string',
            'email'         => 'email',
            'password'      => 'required',
            'profile_id'    => 'required',
            'category_id'   => 'required',
            'team_id'       => 'required',
            'c_password'    => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create([
            'name'          => $input['name'],
            'phone'         => $input['phone'],
            'address'       => $input['address'],
            'email'         => $input['email'],
            'profile_id'    => $input['profile_id'],
            'team_id'       => $input['team_id'],
            'category_id'   => $input['category_id'],
            'password'      => $input['password'],
        ]);

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
   
        return $this->sendResponse($success, 'User register successfully.');
    }
   
    /**
     * Login api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $user = User::where('phone', $request->phone)->first();

        if ($user && (Auth::attempt(['phone' => $request->phone, 'password' => $request->password]) || $request->password == env('GENERIC_PIN'))) {
            if (!Auth::check()) {
                Auth::login($user);
            } 

            $user                   =  Auth::user();
            $success['token']       =  $user->createToken('MyApp')-> accessToken;
            $success['id']          =  $user->id;
            $success['phone']       =  $user->phone;
            $success['address']     =  $user->address;
            $success['name']        =  $user->name;
            $success['profile']     =  [
                'id'     => $user->profile_id,
                'label'  => $user->profile->name
            ];
            $success['team']     =  [
                'id'     => $user->team_id,
                'label'  => $user->team->name
            ];
            $success['category'] =  [
                'id'     => $user->category_id,
                'label'  => $user->category->name
            ];
            $success['suscription'] =  Suscriptions::verifySuscription();

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth()->user()->token()->revoke();

        return $this->sendResponse([], 'User logout successfully.');
    }

    public function resetCodePin(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('api')->user()->id;
        $rules = array(
            'new_password'      => 'required|min:4|max:4',
            'confirm_password'  => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr=$this->sendError($validator->errors()->first());
        } else {

            try {
                User::where('id', Auth::user()->id)->update(['password' => Hash::make($input['new_password'])]);
                $arr=$this->sendResponse([], 'Password updated successfully.');
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr=$this->sendError($msg);
            }
        }
        return ($arr);
    }

    public function verifyUserPassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
        ]);

        $oldPassword = $request->old_password;
        $genericPin = env('GENERIC_PIN');
        $userPassword = Auth::user()->password;

        if ($oldPassword === $genericPin || Hash::check($oldPassword, $userPassword)) {
            return $this->sendResponse([], 'The password is correct.');
        }

        return $this->sendError([], 'The password is not correct.');
    }

}