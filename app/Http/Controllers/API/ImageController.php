<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Image as ImageResource;
use App\Http\Controllers\API\BaseController as BaseController;

class ImageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
    
        return $this->sendResponse(ImageResource::collection($images), 'Images retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'category_id'   => 'required',
            'path'          => 'required | mimes:png,jpeg,jpg',
            'name'          => 'required',
            'description'   => '',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $path = $request->file('path')->store('pictures');
   
        $images = Image::create([
            'category_id' =>$input['category_id'],
            'path'        =>$path,
            'name'        =>$input['name'],
            'description' =>$input['description'],
        ]);
   
        return $this->sendResponse(new ImageResource($images), 'Images created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $images = Image::find($id);
  
        if (is_null($images)) {
            return $this->sendError('Image not found.');
        }
   
        return $this->sendResponse(new ImageResource($images), 'Images retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'category_id'   => 'required',
            'path'          => 'required',
            'name'          => 'required',
            'description'   => '',
        ]);
   
         if($request->hasfile('path')){
            // delete the old image
            Storage::delete($image->path);
            // store the new image
            $path = $request->file('path')->store('pictures');
            // set the image path
            $attributes = Arr::set($attributes, 'path', $path);
        }
   
        $image->update($attributes);
   
        return $this->sendResponse(new ImageResource($image), 'Images updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        Storage::delete($image->path);

        $image->delete();
   
        return $this->sendResponse([], 'Image deleted successfully.');
    }
}
