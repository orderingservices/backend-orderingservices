<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Image as ImageResource;
use App\Http\Controllers\API\BaseController as BaseController;

class AdminController extends BaseController
{
    public function listUserInSameTeam()
    {
        $users = User::where('team_id' , Auth::user()->team_id)
        ->where('profile_id', 3)
        ->get();

        if (is_null($users)) {
            return $this->sendError('Users not found.');
        }
        return $this->sendResponse(UserResource::collection($users), 'Users retrieved successfully.');
    }

    public function listImagesInSameCategory()
    {
         $images = Image::where('category_id' , Auth::user()->category_id)->get();

        if (is_null($images)) {
            return $this->sendError('images not found.');
        }

        return $this->sendResponse(ImageResource::collection($images), 'images retrieved successfully.');
    }
}
