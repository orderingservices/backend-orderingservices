<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Suscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentCallBackController extends Controller
{
    public function createUserSuscription(Request $request)
    {
        $response = $request->data;
        logger()->info('payment confirmation' , $response);

        $user = User::where('phone' , [$response['custom_data']['user_phone']])->first();
        
        if ($user) {
            Suscription::create([
                'user_id'       => $user->id,
                'month_number'  => Carbon::now()->subMonth()->format('Y-m'),
                'month_name'    => Carbon::now()->subMonth()->format('M'),
                'amount'        => env('SUBSCRIPTION_AMOUNT')
            ]);
        }
    }
}
