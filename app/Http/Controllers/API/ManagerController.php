<?php

namespace App\Http\Controllers\API;

use DB;
use App\Models\User;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\Order as OrderResource;
use App\Http\Controllers\API\BaseController as BaseController;

class ManagerController extends BaseController
{
    public function listProducts($id)
    {
        $product = User::find($id)->products;

        if (is_null($product)) {
            return $this->sendError('Products not found.');
        }
        return $this->sendResponse(ProductResource::collection($product), 'Products retrieved successfully.');   
    }

    public function listOrders($id , $start_at , $end_at)
    {
        $order = DB::table('orders')
                    ->where('user_id',$id)
                    ->whereBetween('created_at', [$start_at ,  $end_at])
                    ->orderBy('id','desc')
                    ->get();

        if (is_null($order)) {
            return $this->sendError('Order not found.');
        }
        return $this->sendResponse(OrderResource::collection($order), 'Orders retrieved successfully.');
    }

    public function balance($id, $start_at, $end_at)
    {
        $amount = DB::table('orders')
                    ->where('user_id',$id)
                    ->whereBetween('created_at', [$start_at ,  $end_at])
                    ->sum('amount'); 

        return [
            'balance'  => $amount
        ];
    }
}
