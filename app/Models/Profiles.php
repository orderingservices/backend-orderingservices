<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profiles extends Model
{
    use HasFactory;

    protected $guarded = [];

     public function user(){

        return $this->HasOne(User::class);
    }
}
