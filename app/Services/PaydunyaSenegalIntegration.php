<?php


namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\Suscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\BaseController as BaseController;

class PaydunyaSenegalIntegration extends BaseController
{
    public static function checkoutInvoice($userPhone)
    {

        $client = new Client();

        // Les données à envoyer
        $headers = [
            'PAYDUNYA-MASTER-KEY'   => env('PAYDUNYA_MASTER_KEY'),
            'PAYDUNYA-PRIVATE-KEY'  => env('PAYDUNYA_PRIVATE_KEY'),
            'PAYDUNYA-TOKEN'        => env('PAYDUNYA_TOKEN')
        ];
        $data = [
            "invoice" => [
                "total_amount" => env('SUBSCRIPTION_AMOUNT'),
                "description" => "Chaussure VANS dernier modèle"
            ],
            "store" => [
                "name"  => "Magasin le Choco", 
            ],

            "custom_data" => [
                "user_phone"    => $userPhone
            ]
        ];

        try {
            $response = $client->post(env('PAYDUNYA_BASE_URL').'checkout-invoice/create', [
                'headers' => $headers,
                'json'   => $data,
            ]);

            $body = $response->getBody();
            $content = $body->getContents();
            
            $response_data = json_decode($content, true);

            return $response_data['token'];

        } catch (\Exception $e) {
            
            return "Une erreur s'est produite : " . $e->getMessage();
        }

    }

    public static function orangeMoneyIntegration($phone,$ussd_code,$userPhone)
    {
        $client = new Client();

        $data = [
            "customer_name"         => "OMEGA CAISSE",
            "customer_email"        => "omegacaisse@gmail.com",
            "phone_number"          => $phone,
            "authorization_code"    => $ussd_code,
            "invoice_token"         => PaydunyaSenegalIntegration::checkoutInvoice($userPhone),
        ];

        try {
            $response = $client->post(env('PAYDUNYA_BASE_URL').'softpay/orange-money-senegal', [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json'   => $data,
            ]);

            $body = $response->getBody();
            $content = $body->getContents();
            
            $response_data = json_decode($content, true);

            return [
                    'success'   => true,
                    'data'      => $response_data,
                    'message'   => 'Paiement effectué avec succès'
                ];

        } catch (\Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $body = $response->getBody();
                $content = $body->getContents();
                $response_data = json_decode($content, true);

                return [
                    'success'   => false,
                    'data'      => $response_data,
                    'message'   => 'Une erreur de paiement est effectué.'
                ];
            } else {
                return [
                    'success'   => false,
                    'data'      => $e->getMessage(),
                    'message'   => 'Une erreur de paiement est effectué.'
                ];
            }
        }
    }

    public static function waveIntegration($phone,$userPhone)
    {

        $client = new Client();

        $data = [
            "wave_senegal_fullName"         => "OMEMGA CAISSE",
            "wave_senegal_email"            => "omegacaisse@gmail.com",
            "wave_senegal_phone"            => $phone,
            "wave_senegal_payment_token"    => PaydunyaSenegalIntegration::checkoutInvoice($userPhone),
        ];

        try {
            $response = $client->post(env('PAYDUNYA_BASE_URL').'softpay/wave-senegal', [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json'   => $data,
            ]);

            $body = $response->getBody();
            $content = $body->getContents();
            
            $response_data = json_decode($content, true);

            return [
                    'success'   => true,
                    'data'      => $response_data,
                    'message'   => 'Paiement Initié avec succès'
                ];

        } catch (\Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $body = $response->getBody();
                $content = $body->getContents();
                $response_data = json_decode($content, true);

                // Traitez la réponse d'erreur ici
                return [
                    'success'   => false,
                    'data'      => $response_data,
                    'message'   => 'Une erreur de paiement est effectué.'
                ];
            } else {
                return [
                    'success'   => false,
                    'data'      => $e->getMessage(),
                    'message'   => 'Une erreur de paiement est effectué.'
                ];
            }
        }
    }
}
