<?php


namespace App\Services;

use Carbon\Carbon;
use App\Models\Suscription;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class Suscriptions
{
    public static function verifySuscription()
    {
        $lastMonthSuscription = Suscription::where('user_id', Auth::user()->id)->where('month_number', Carbon::now()->subMonth()->format('Y-m'))->first();

        $beforeLastMonthSuscription = Suscription::where('user_id', Auth::user()->id)->where('month_number', Carbon::now()->subMonth(2)->format('Y-m'))->first();

        //between 1 at 5 of every month notify client to paid. 
        //(dans ce cas on verifie si l'utilisateur a payé l'avant dernuier mais n'a pas paye de dernier mois : on bloque pas l'application et on lui notifie)
        //On verifie aussi si c'est un compte de test avec l'e-mail falloutall2020@gmail.com : il paie pas d'abonnement et aussi on lui notifi
        if((Carbon::now()->format('d') >= 1 and Carbon::now()->format('d') <= 6 and !$lastMonthSuscription and $beforeLastMonthSuscription) || Auth::user()->email == "falloutall2020@gmail.com")
            return [
                "is_subscribed" => true,
                "is_notified"   => true
            ];
            

        //if user is created in the current or last month or user paid last month or user is admin
        if ((Carbon::parse(Auth::user()->created_at)->format('Y-m') == Carbon::now()->format('Y-m')) || (Carbon::parse(Auth::user()->created_at)->format('Y-m') == Carbon::now()->subMonth()->format('Y-m')) || $lastMonthSuscription || Auth::user()->profile_id == 2)  
            return [
                "is_subscribed"  => true,
                "is_notified"    => false,
            ]; 
            

        // if user not paid
        return [
            "is_subscribed" => false,
            "is_notified"   => true,
        ];
    }
}