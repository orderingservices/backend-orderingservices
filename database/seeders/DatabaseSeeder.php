<?php

namespace Database\Seeders;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // DB::table('categories')->insert([
        //     ['name' => 'Fast Food'],
        //     ['name' => 'Restaurant'],
        //     ['name' => 'Pressing'],
        //     ['name' => 'Admin'],
        // ]);


        // DB::table('profiles')->insert([
        //     ['name' => 'Admin'],
        //     ['name' => 'Superviseur'],
        //     ['name' => 'Vendeur'],
        // ]);

        // DB::table('teams')->insert([
        //     ['name' => 'Team Fast Food'],
        //     ['name' => 'Team Restaurant'],
        //     ['name' => 'Team Pressing'],
        //     ['name' => 'Admin'],
        // ]);

        // DB::table('users')->insert([
        //     ['profile_id' => 2,'team_id' => 1,'category_id' => 1,'name' => 'FastFood Admin','phone' => 771111111,'address' => 'pikine sud','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 3,'team_id' => 1,'category_id' => 1,'name' => 'ouba pikine','phone' => 781111111,'address' => 'Ouest Foire','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 2,'team_id' => 2,'category_id' => 2,'name' => 'Restaurant Admin','phone' => 772222222,'address' => 'Medina','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 3,'team_id' => 2,'category_id' => 2,'name' => 'Restaurant B','phone' => 782222222,'address' => 'Liberté 6','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 2,'team_id' => 3,'category_id' => 3,'name' => 'Pressing Admin','phone' => 773333333,'address' => 'Liberté 5','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 3,'team_id' => 3,'category_id' => 3,'name' => 'Pressing B','phone' => 783333333,'address' => 'Grand Medine','password' => bcrypt(1234), 'email' => 'falloutall2020@gmail.com','created_at' => Carbon::now()],
        //     ['profile_id' => 1,'team_id' => 4,'category_id' => 4,'name' => 'Admin','phone' => 770000000,'address' => 'Nord Foire','password' => bcrypt(1020), 'email' => 'fallou@admin.com','created_at' => Carbon::now()],
        // ]);
    }
}
