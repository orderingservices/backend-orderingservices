<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\TeamController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\ImageController;
use App\Http\Controllers\API\ManagerController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\PaymentCallBackController;
use App\Http\Controllers\API\PaymentSuscription;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);
   
Route::middleware('auth:api')->group( function () {
    Route::post('logout', [RegisterController::class, 'logout']);
    Route::resource('products', ProductController::class);
    Route::resource('teams', TeamController::class);
    Route::resource('orders', OrderController::class);
    Route::resource('images', ImageController::class);
    Route::resource('categories', CategoryController::class);
    Route::get('team/users', [AdminController::class , 'listUserInSameTeam']);
    Route::get('/user/{id}/products', [ManagerController::class , 'listProducts']);
    Route::get('/user/{id}/orders/{start_at}/{end_at}', [ManagerController::class , 'listOrders']);
    Route::get('/category/images', [AdminController::class , 'listImagesInSameCategory']);
    Route::get('/user/{id}/balance/{start_at}/{end_at}', [ManagerController::class , 'balance']);
    Route::post('/user/{id}/verify/password', [RegisterController::class , 'verifyUserPassword']);
    Route::post('/user/{id}/reset/password', [RegisterController::class , 'resetCodePin']);
    Route::post('/suscription/init/payment', [PaymentSuscription::class , 'initPayment']);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('paydunya/call-back/confirm/payment', [PaymentCallBackController::class , 'createUserSuscription']);
